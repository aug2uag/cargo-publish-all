use std::io::{stdin, stdout, Write};
use std::process::exit;

use cargo::{
    core::{Package, Verbosity, Workspace},
    ops::PublishOpts,
    Config,
};

use cargo_publish_all::{
    compute_package_order, fetch_crates_io_versions, package_modes, publish_all, verify_all, Error,
    Result,
};
use clap::{App, Arg};
use dunce::canonicalize;
use slog::*;

pub fn cli() -> App<'static, 'static> {
    App::new("cargo-publish-all")
        .version("0.2")
        .author("Thomas Schaller <torkleyy@gmail.com>")
        .about("Upload workspace packages to crates.io")
        .arg(
            Arg::with_name("token")
                .long("token")
                .help("Token to use when uploading")
                .value_name("TOKEN"),
        )
        .arg(
            Arg::with_name("allow-dirty")
                .long("allow-dirty")
                .help("Allow dirty working directories to be packaged"),
        )
        .arg(
            Arg::with_name("manifest-path")
                .long("manifest-path")
                .help("Path to Cargo.toml"),
        )
        .arg(
            Arg::with_name("dry-run")
                .long("dry-run")
                .help("Perform all checks without uploading"),
        )
        .arg(
            Arg::with_name("yes")
                .long("yes")
                .help("Confirm publishing without interaction"),
        )
        .arg(
            Arg::with_name("verbose")
                .long("verbose")
                .help("Show more output"),
        )
}

fn run(log: &slog::Logger) -> Result<()> {
    let matches = cli().get_matches();

    let config = Config::default()?;

    if matches.is_present("verbose") {
        config.shell().set_verbosity(Verbosity::Verbose);
    } else {
        config.shell().set_verbosity(Verbosity::Normal);
    }

    let manifest_path = matches.value_of("manifest-path").unwrap_or("Cargo.toml");
    let manifest_path = canonicalize(manifest_path).map_err(|_| Error::NoCargoToml)?;
    let ws = Workspace::new(&manifest_path, &config)?;

    let packages: Vec<Package> = compute_package_order(&ws)?;
    let versions = fetch_crates_io_versions(&ws, &packages)?;
    let modes = package_modes(&packages, &versions)?;

    if modes.iter().all(|m| m.is_skip()) {
        info!(log, "Everything published already. Exiting.");
        return Ok(());
    }

    verify_all(&ws, &packages, &modes, matches.is_present("allow-dirty"))?;

    println!("Summary:");
    for (i, pkg) in packages.iter().enumerate() {
        let mode = &modes[i];

        print!("{}: {}", pkg.name(), pkg.version());

        if mode.is_skip() {
            print!(" (skipped)");
        }

        println!();
    }

    if matches.is_present("dry-run") {
        return Ok(());
    }

    if !matches.is_present("yes") {
        print!("Continue? [y/N] ");
        stdout().flush().unwrap();
        let mut s = String::new();
        stdin().read_line(&mut s).unwrap();

        if !s.starts_with("y") && !s.starts_with("Y") {
            println!("Canceled");

            exit(2);
        }
    }

    let opts = PublishOpts {
        config: ws.config(),
        token: matches.value_of("token").map(String::from),
        index: None,
        verify: true,
        allow_dirty: matches.is_present("allow-dirty"),
        jobs: None,
        target: None,
        dry_run: false,
        registry: None,
    };

    publish_all(&ws, &packages, &modes, &opts)?;

    Ok(())
}

fn main() {
    use failure::Fail;
    let log = cargo_publish_all::logger::init_logger();
    match run(&log) {
        Ok(_) => {}
        Err(e) => {
            use std::fmt::Write;

            let mut msg = String::new();
            let _ = writeln!(msg, "Error: {}", e);

            for c in Fail::iter_causes(&e) {
                let _ = writeln!(msg, "caused by: {}", c);
            }

            error!(log, "{}", msg);

            // let config = Config::default();

            // if let Ok(config) = config {
            //     let _ = config.shell().error(msg);
            // } else {
            //     eprintln!("{}", msg);
            // }

            exit(1);
        }
    }
}
